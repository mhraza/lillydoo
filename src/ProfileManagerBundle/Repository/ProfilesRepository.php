<?php

namespace ProfileManagerBundle\Repository;

use Symfony\Bridge\Doctrine\RegistryInterface;
use ProfileManagerBundle\Entity\Profiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Psr\Container\ContainerInterface;

/**
 * ProfilesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProfilesRepository extends ServiceEntityRepository
{
    /**
     * @ContainerInterface
     */
    private $container;

    /**
     * ProfilesRepository constructor.
     * @param RegistryInterface $registry
     * @param ContainerInterface $container
     */
    public function __construct(RegistryInterface $registry, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($registry, Profiles::class);
    }

    /**
     * @param array $attributes
     * @return Profiles
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addProfile(array $attributes): ?Profiles
    {
        $profile = new Profiles();
        $profile->setFirstName($attributes['first_name']);
        $profile->setLastName($attributes['last_name']);
        $profile->setStreet($attributes['street']);
        $profile->setZip($attributes['zip']);
        $profile->setCity($attributes['city']);
        $profile->setCountry($attributes['country']);
        $profile->setPhoneNunber($attributes['phone']);
        $profile->setBirthday(new \DateTime($attributes['birthday']));
        $profile->setEmailAddress($attributes['email']);
        $profile->setPictureUrl($attributes['picture']);
        $this->_em->persist($profile);
        $this->_em->flush($profile);

        return $profile;

    }

    /**
     * @param array $attributes
     * @return object|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editProfile(array $attributes)
    {
        $profile = $this->find($attributes['id']);
        $profile->setFirstName($attributes['first_name']);
        $profile->setLastName($attributes['last_name']);
        $profile->setStreet($attributes['street']);
        $profile->setZip($attributes['zip']);
        $profile->setCity($attributes['city']);
        $profile->setCountry($attributes['country']);
        $profile->setPhoneNunber($attributes['phone']);
        $profile->setBirthday(new \DateTime($attributes['birthday']));
        $profile->setEmailAddress($attributes['email']);
        if ($attributes['picture'] != "" || $attributes['picture'] != null) {
            $profile->setPictureUrl($attributes['picture']);
        }
        $this->_em->persist($profile);
        $this->_em->flush($profile);

        return $profile;

    }

    /**
     * @param Profiles $profile
     */
    public function removeProfile(Profiles $profile)
    {
        try {
            $this->_em->remove($profile);
            $this->_em->flush($profile);
        } catch (\Doctrine\ORM\ORMException | \Doctrine\ORM\OptimisticLockException $ex) {
            throw new $ex;
        }
    }

    /**
     * @param int $from
     * @param int $to
     * @return mixed
     */
    public function getProfiles(int $from, int $to)
    {
        $query = $this->_em->createQueryBuilder()
            ->addSelect('profiles')
            ->from('LillyDooProfileManagerBundle:Profiles', 'profiles')
            ->getQuery();
        $paginator  = $this->container->get('knp_paginator');
        $profiles = $paginator->paginate($query, $from, $to
        );


        return $profiles;

    }
}
